MAKEFLAGS = -r

PYS?=$(wildcard *.py)
SVGS?=$(PYS:.py=.svg)
PIP_OPTS ?= --upgrade --upgrade-strategy=eager

ifeq ($(VIRTUAL_ENV),)
  PIP_OPTS += --user
endif

all: $(SVGS)

setup: requirements.txt
	pip install $(PIP_OPTS) -r $?

%.svg: %.py
	python -OO $<

clean:
	rm -f $(SVGS)
