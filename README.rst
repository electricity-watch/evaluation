Evaluation of `electricity.watch <https://electricity.watch>`__.

Specifically of interest are:

* input parameters,
* output values,
* and effectiveness of deployed mechanisms

Code is short-lived and ran once; expect quick-and-dirty scripts.
