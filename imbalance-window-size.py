#!/usr/bin/env python3

from os import environ
from os.path import splitext
from time import sleep
import sys
import warnings

# suppress pandas future warning
warnings.simplefilter(action='ignore', category=FutureWarning)
# suppress beautiful soup warning
from bs4 import XMLParsedAsHTMLWarning
warnings.simplefilter(action='ignore', category=XMLParsedAsHTMLWarning)

from entsoe import EntsoePandasClient
import matplotlib.pyplot as plt
from pandas import Timestamp, Timedelta

assert 'ENTSOE_API_KEY' in environ, 'need ENTSOE_API_KEY as env var'

client = EntsoePandasClient(
  api_key=environ['ENTSOE_API_KEY'],
  retry_count=3,
  retry_delay=1,
)

end = Timestamp(Timestamp.now().date(), tz='UTC') - Timedelta(days=1)

for area in ('PL', 'DE_LU', 'FR', 'SE'):
  means = []
  data = client.query_imbalance_volumes(
    area, start=end-Timedelta(days=1000), end=end
  )
  intervals = range(-len(data.groupby(data.index.date)), 0)
  for days in intervals:
    mean = data.loc[end+Timedelta(days=days):end].mean().iloc[0]
    print(f'area {area}, days {days}, mean {mean}')
    means.append(mean)
  plt.plot(intervals, means, label=area)

plt.legend()
plt.savefig(f'{splitext(sys.argv[0])[0]}.svg')
