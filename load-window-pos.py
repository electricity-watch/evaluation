#!/usr/bin/env python3

from os import environ
from os.path import splitext
from time import sleep
import sys
import warnings

# suppress pandas future warning
warnings.simplefilter(action='ignore', category=FutureWarning)
# suppress beautiful soup warning
from bs4 import XMLParsedAsHTMLWarning
warnings.simplefilter(action='ignore', category=XMLParsedAsHTMLWarning)

from entsoe import EntsoePandasClient
import matplotlib.pyplot as plt
from pandas import Timestamp, Timedelta

assert 'ENTSOE_API_KEY' in environ, 'need ENTSOE_API_KEY as env var'

client = EntsoePandasClient(
  api_key=environ['ENTSOE_API_KEY'],
  retry_count=3,
  retry_delay=1,
)

max_days_back = 1000
window_size = 100
half_window_size = int(window_size / 2)
assert window_size % 2 == 0
assert window_size < max_days_back
end = Timestamp(Timestamp.now().date(), tz='UTC') - Timedelta(days=1)
start = end - Timedelta(days=max_days_back)

for area in ('PL', 'DE_LU', 'FR', 'SE'):
  means = []
  data = client.query_load(area, start=start, end=end)
  centers = range(
    -len(data.groupby(data.index.date)) + half_window_size,
    -half_window_size
  )
  for center in centers:
    mean_start = end + Timedelta(days=center-half_window_size)
    mean_end = mean_start + Timedelta(days=window_size)
    mean = data.loc[mean_start:mean_end]['Actual Load'].mean()
    print(f'area {area}, center {center}, mean {mean}')
    means.append(mean)
  plt.plot(centers, means, label=area)

plt.legend()
plt.savefig(f'{splitext(sys.argv[0])[0]}.svg')
