#!/usr/bin/env python3

from os import environ
from os.path import splitext
from time import sleep
import sys
import warnings

# suppress pandas future warning
warnings.simplefilter(action='ignore', category=FutureWarning)
# suppress beautiful soup warning
from bs4 import XMLParsedAsHTMLWarning
warnings.simplefilter(action='ignore', category=XMLParsedAsHTMLWarning)

from entsoe import EntsoePandasClient
import matplotlib.pyplot as plt
from pandas import Timestamp, Timedelta

assert 'ENTSOE_API_KEY' in environ, 'need ENTSOE_API_KEY as env var'

GOOD_SOURCES = {
  'Geothermal',
  'Other renewable',
  'Solar',
  'Wind Offshore',
  'Wind Onshore',
}


BAD_SOURCES = {
  'Biomass',
  'Fossil Brown coal/Lignite',
  'Fossil Coal-derived gas',
  'Fossil Gas',
  'Fossil Hard coal',
  'Fossil Oil',
  'Fossil Oil shale',
  'Fossil Peat',
  'Hydro Pumped Storage',
  'Hydro Run-of-river and poundage',
  'Hydro Water Reservoir',
  'Marine', # what is this?
  'Nuclear',
  'Other',
  'Waste',
}


client = EntsoePandasClient(
  api_key=environ['ENTSOE_API_KEY'],
  retry_count=3,
  retry_delay=1,
)

max_days_back = 1000
window_size = 200
half_window_size = int(window_size / 2)
assert window_size % 2 == 0
assert window_size < max_days_back
end = Timestamp(Timestamp.now().date(), tz='UTC') - Timedelta(days=1)
start = end - Timedelta(days=max_days_back)

for area in ('PL', 'DE_LU', 'FR', 'SE'):
  shares = []
  data = client.query_generation(area, start=start, end=end)

  if data.columns.nlevels > 1:
    data = data.drop('Actual Consumption', axis=1, level=1)
    assert {*data.columns.get_level_values(1)} == {'Actual Aggregated'}
    data = data.droplevel(1, axis=1)

  assert set() == set(data.columns) - GOOD_SOURCES - BAD_SOURCES

  centers = range(
    -len(data.groupby(data.index.date)) + half_window_size,
    -half_window_size
  )
  for center in centers:
    mean_start = end + Timedelta(days=center-half_window_size)
    mean_end = mean_start + Timedelta(days=window_size)
    sliced = data[mean_start:mean_end]
    available_good = [s for s in GOOD_SOURCES if s in sliced.keys()]
    share = sliced[available_good].sum().sum() / sliced.sum().sum()
    print(f'area {area}, center {center}, share {share}')
    shares.append(share)

  plt.plot(centers, shares, label=area)

plt.legend()
plt.savefig(f'{splitext(sys.argv[0])[0]}.svg')
